#include <unity.h>
#include "1920_1631COMP_2Post2Symposium.cpp"

void test_first_Test(void) {
    TEST_ASSERT_EQUAL(33, 33);
}

void test_pins_variables(void) { // Checks all variables are set to the correct pins as component guide.
    TEST_ASSERT_EQUAL(PIR, 26);
    TEST_ASSERT_EQUAL(BLUE_LED, 33);
    TEST_ASSERT_EQUAL(RED_LED, 25);
    TEST_ASSERT_EQUAL(GREEN_LED, 32);
    TEST_ASSERT_EQUAL(BUTTON, 14);
    TEST_ASSERT_EQUAL(BUZZER, 16);
    TEST_ASSERT_EQUAL(DHTSENSOR, 15);
    TEST_ASSERT_EQUAL(SDCARDREADER, 18);
    TEST_ASSERT_EQUAL(POTENTIOMETER, 34);
}

void test_led(void) {
    TEST_ASSERT_TRUE(LED_Setup()); // checks leds are set up and working and also checks PWM is working. 
}
void test_OLED(void) {
    TEST_ASSERT_TRUE(OLED_Setup());
}
void test_SensorSetup(void) {
    TEST_ASSERT_TRUE(sensorSetup());
}

void process(){
    UNITY_BEGIN();
    RUN_TEST(test_first_Test);
    RUN_TEST(test_pins_variables); // Will return true if test pins executes fine. 
    RUN_TEST(test_led);
    UNITY_END();
}

#ifdef ARDUINO

#include <Arduino.h>

void setup() {
    delay(2000);
    process();
    
}

void loop() {
    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(500);
}

#else 

int main(int argc, char **argv) {
    process();
    return 0;
}

#endif