/*
 * Humidity.h
 *
 *  Created on: 6 Feb 2020
 *      Author: cmparoge
 */

#ifndef HUMIDITYSTATE_H_
#define HUMIDITYSTATE_H_

enum HumidityState {
	HUM_GREEN,
	HUM_AMBER
};



#endif /* HUMIDITYSTATE_H_ */
