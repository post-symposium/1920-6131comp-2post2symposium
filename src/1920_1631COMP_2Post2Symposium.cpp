#include "Arduino.h"
#include "DHTesp.h"
#include "TemperatureState.h"
#include "HumidityState.h"
#include "LEDState.h"
#include "PIRState.h"
#include "Ticker.h"
#include <Wire.h>
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"
#include "Montserrat_Light10pt7b.h"
#include "Montserrat_Light8pt7b.h"
#include "bitmap.h"
#include "SD.h"
#include "WiFi.h"
#include "HTTPClient.h"

//The setup function is called once at startup of the sketch


//************************************ Define Input Devices ************************************
#define PIR 26
#define BLUE_LED 33
#define RED_LED 25
#define GREEN_LED 32
#define BUTTON 14
#define BUZZER 16
#define DHTSENSOR 15
#define SDCARDREADER 18
#define POTENTIOMETER 34

//**** OLED PARAMS ***********
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)


//************************************ End of Define Input Devices ************************************

// ********** WIFI ************
#define ssid "Adam's iPhone"
#define password "12345678"
// ****************************

//**** setting PWM properties ***********
const int RED_ledChannel = 0;
const int GREEN_ledChannel = 1;


//************************************ Declaring State ************************************
TemperatureState tempState;
TemperatureState oldTempState;
HumidityState humState;
HumidityState oldHumState;
LEDState LEDState;
PIRState PIRState;
//************************************ Declaring State ************************************

//************************************ Global Variables ***********************************
unsigned long startMillis;
unsigned long currentMillis;
unsigned long tempMillis;
unsigned long amberAlarmMillis;
unsigned long redAlarmMillis;
unsigned long buzzerMillis;
long timeIncrements[] = {5000, 10000, 30000, 60000, 120000, 300000};
int pos = 0; //array position
unsigned long period = timeIncrements[pos];
//************************************ End of global variables ****************************

//************************************** Button **********************                                                                                                                                                                                                                                                                                        ************
#define debounce 100 // ms debounce period to prevent flickering when pressing or releasing the button
#define holdTime 2000 // ms hold period: how long to wait for press+hold event

// Button variables
int buttonVal = 0; // value read from button
int buttonLast = 0; // buffered value of the button's previous state
long buttonDownTime;
long lastButtonDown;
bool buttonHeld = false;

//************************************ Buzzer *********************************************
int freq = 2000;
int channel = 4;
int resolution = 8;
//************************************ Buzzer End *****************************************


//************************************ SD Card ********************************************
const int chipSelect = 5;
File myFile;
//************************************ SD Card End ****************************************

bool wifiEnabled;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

DHTesp dht; // Initialising DHTESP Object
void tempTask(void *pvParameters);
bool getTemperature();
void triggerGetTemp();
bool initTemp();
bool initPIR();
void LED_PWM_Setup();

/** Task handle for the light value read task */
TaskHandle_t tempTaskHandle = NULL;
/** Ticker for temperature reading */
Ticker tempTicker;
/** Comfort profile */
ComfortState cf;
/** Flag if task should run */
bool tasksEnabled = false;

bool sensorSetup()
{
	initTemp();
	initPIR();
	return true;
}

bool OLED_Setup(){
	if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
	    Serial.println(F("SSD1306 allocation failed"));
	    return false;
	  } else {
		  Serial.println("OLED initiated");
		  display.clearDisplay();
		  display.setTextColor(WHITE);
		  display.setFont(&Montserrat_Light8pt7b);
		  display.setCursor(5,20);
		  display.println(F("2Post"));
		  display.setCursor(15,40);
		  display.println(F("2Symposium"));
		  display.display();
		  return true;
	  }
}

bool LED_Setup(){
	pinMode(RED_LED, OUTPUT);
	pinMode(BLUE_LED, OUTPUT);
	pinMode(GREEN_LED, OUTPUT);

	digitalWrite(BLUE_LED, HIGH);
	delay(500);
	digitalWrite(BLUE_LED, LOW);
	digitalWrite(RED_LED, HIGH);
	delay(500);
	digitalWrite(RED_LED, LOW);
	digitalWrite(GREEN_LED, HIGH);
	delay(500);
	digitalWrite(GREEN_LED, LOW);

	Serial.println("LED initiated");

	LED_PWM_Setup();
	Serial.println("PWM initiated");
	return true;


}

void LED_PWM_Setup(){
	//configure LED PWM functionalities
		ledcSetup(RED_ledChannel, 5000, 8);
		ledcSetup(GREEN_ledChannel, 5000, 8);
		//include channel to GPIP for controlling
		ledcAttachPin(RED_LED, RED_ledChannel);
		ledcAttachPin(GREEN_LED, GREEN_ledChannel);
}

bool wifiSetup() {
	int count = 0;
	WiFi.begin(ssid, password);
	 while (WiFi.status() != WL_CONNECTED && count <= 5) {
	   delay(250);
	   Serial.print(".");
	   count++;
	 }
	 if(WiFi.status() == WL_CONNECTED){
		 Serial.print("Connected as : ");
	 	 Serial.println(WiFi.localIP());
	 	 return true;
	 } else {
		 Serial.println("Wifi Not Connected.");
		 return false;
	 }
}

bool initPIR(){
	pinMode(PIR, INPUT);
	Serial.println("PIR initiated");
	return true;
}

bool initTemp() {
  //byte resultValue = 0;
  // Initialize temperature sensor
	dht.setup(15, DHTesp::DHT11);
	Serial.println("DHT initiated");
  return true;
}


float temperature;
float humidity;

bool getTemperature() {
	// Reading temperature for humidity takes about 250 milliseconds!
	// Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
  TempAndHumidity newValues = dht.getTempAndHumidity();
	// Check if any reads failed and exit early (to try again).
	if (dht.getStatus() != 0) {
		Serial.println("DHT11 error status: " + String(dht.getStatusString()));
		return false;
	}

	temperature = newValues.temperature;
	humidity = newValues.humidity;
	float heatIndex = dht.computeHeatIndex(newValues.temperature, newValues.humidity);
	float dewPoint = dht.computeDewPoint(newValues.temperature, newValues.humidity);
	//float cr = dht.getComfortRatio(cf, newValues.temperature, newValues.humidity);



	return true;
}

void displayDHTStats() {
	Serial.println("Temperature: " + String(temperature) + " Humidity: " + String(humidity));
}

void turnOffLED(){
	digitalWrite(BLUE_LED, LOW);
	ledcWrite(RED_ledChannel, 0);
	ledcWrite(GREEN_ledChannel, 0);
}


void LED_Status(float temp, float hum) {
	if(temp != 0) {
		if(temp >= 18 && temp <= 23){
			Serial.println("Temperature LED GREEN");
			tempState = TemperatureState::TEMP_GREEN;

		} else {
			Serial.println("Temperature LED Amber");
			tempState = TemperatureState::TEMP_AMBER;
		}
	}
	if(hum != 0) {
		 if(hum >= 35 && hum <= 60){
			 Serial.println("Humidity LED GREEN");
			 humState = HumidityState::HUM_GREEN ;
		} else {
			 Serial.println("Humidity LED Amber");
			 humState = HUM_AMBER;
		}
	}
	turnOffLED();
	if(tempState == TEMP_GREEN && humState == HUM_GREEN ){
		LEDState = GREEN;
		ledcWrite(GREEN_ledChannel, 255);
	} else if (tempState == TEMP_AMBER && humState == HUM_AMBER) {
		LEDState = RED;
		ledcWrite(RED_ledChannel, 255);
	} else if (tempState == TEMP_AMBER || humState == HUM_AMBER){
		LEDState = AMBER;
		ledcWrite(RED_ledChannel, 255);
		ledcWrite(GREEN_ledChannel, 40);

		//digitalWrite(BLUE_LED, HIGH);
	}

	if(oldTempState != tempState){
		oldTempState = tempState;
		Serial.println("New Temperature State: " + String(tempState));
	}
	if(oldHumState != humState){
		oldHumState = humState;
		Serial.println("New Humidity State: " + String(humState));
	}
}

int pirRead;
int pirInterval = 0;

void PIR_Read(){

	pirRead = digitalRead(PIR);
}

void PIR_Status(){

	//Serial.println("PIR :" + String(pirRead));



	if(PIRState == OCCUPIED){
		pirInterval++;
		if(pirInterval == 2){
			PIRState = VACANT;
			Serial.println("Vacant");
			pirInterval = 0;
		}
	}

	if(pirRead == HIGH && PIRState != OCCUPIED){
		PIRState = OCCUPIED;
		Serial.println("Occupied");
	}

}
int val = 0;

void Screen(){
	val = analogRead(POTENTIOMETER);
	//Serial.println("PETENT: " + String(val));
	display.clearDisplay();

	if(PIRState == OCCUPIED){
		display.drawBitmap(105, 50, user_icon, 16, 16, WHITE);
	}

	if(val <= 1500) {
		  display.setCursor(0,15);             // Start at top-left corner
		  display.println(F("Temperature"));

		  display.setCursor(40, 40);
		  display.setTextColor(WHITE);
		  display.print(String(temperature));
		  display.drawCircle(85, 30, 3, WHITE);
		  display.display();
	}

	if(val > 1500) {
			  display.setCursor(0,15);             // Start at top-left corner
			  display.println(F("Humidity"));

			  display.setCursor(40, 40);
			  display.setTextColor(WHITE);
			  display.print(String(humidity) + "%");
			  display.display();
		}
}

void sdSetup(){
	Serial.println("Initializing SD card...");
	pinMode(chipSelect, OUTPUT);
	if (!SD.begin(chipSelect)) {
		Serial.println("initialization failed!");
		return;
	}
	Serial.println("initialization done.");

	if (SD.exists("/2post2symposium.txt")) {
	    Serial.println("example.txt exists.");
	  } else {
	    Serial.println("example.txt doesn't exist.");
	    myFile = SD.open("/2post2symposium.txt", FILE_WRITE);
	    myFile.close();
	  }
}

void sdCard(){
	String timeDate;
	if(wifiEnabled) {
		HTTPClient http;
		 String url = "http://172.20.10.2:3000/time";

		 //Serial.println(url);

		 http.begin(url);
		 	//Serial.println(http.getString());
		    int httpResponseCode = http.GET();
		    if(httpResponseCode > 0) {
		      String response = http.getString();
		      //Serial.println(httpResponseCode);
		      //Serial.println(response);
		      timeDate = response;
		    } else {
		      Serial.println("Error");
		    }
		  http.end();
	}

	myFile = SD.open("/2post2symposium.txt", FILE_APPEND);
	if (myFile) {
		String FileInfo = String(timeDate) + " - Temperature: " + String(temperature) + " Humidity: " + String(humidity) + " Occupied: " + PIRState + " Humidity State: " + humState;
		myFile.println(FileInfo);
		myFile.close();
	} else {
		// if the file didn't open, print an error:
		Serial.println("error opening test.txt");
	}

	// ***** Reads file to serial *******
//	myFile = SD.open("/2post2symposium.txt");
//	Serial.println("********* FILE *********");
//	    while (myFile.available()) {
//	      Serial.write(myFile.read());
//	    }
//	    // close the file:
//	    myFile.close();
//	    Serial.println("********* FILE END *********");
	// ***********************************
}

void buzzerSetup(){
	ledcSetup(channel, freq, resolution);
	ledcAttachPin(BUZZER, channel);
	ledcWriteTone(channel, 2000);
	delay(500);
	ledcWriteTone(channel, 3000);
	delay(500);
	ledcWriteTone(channel, 1000);
	delay(500);
	ledcWriteTone(channel, 0);
}

bool redAlarm, amberAlarm = false;
bool alarmSnoozed = false;

void buzzerLoop() {
	
	switch (LEDState)
	{
	case AMBER:
		amberAlarm = true;
		break;
	case RED: 
		redAlarm = true;
		break;
	default:
		break;
	}




	// if (PIRState == OCCUPIED) {
		
	// 	Serial.println(PIRState);

	// 	switch (LEDState) {

	// 	case AMBER: {
	// 		int buzzerPeriod = 30000;
			
	// 		if (currentMillis - buzzerMillis >= buzzerPeriod) {	
	// 			ledcWriteTone(channel, 1000);
	// 			buzzerMillis = currentMillis;

	// 			while (buttonHeld == true){
	// 				//If button is held then it will pause the buzzer
	// 				int pausePeriod = 10000;
	// 				if (currentMillis - buzzerMillis >= pausePeriod) {
	// 				ledcWriteTone(channel, 0);
	// 				buzzerMillis = currentMillis;
	// 				} 
	// 			} 
	// 		}
	// 		else {
	// 			ledcWriteTone(channel, 0);
	// 		}
	// 		break;
	// 	}

	// 	case RED: {
	// 		int buzzerPeriod = 5000;

	// 		if (currentMillis - buzzerMillis >= buzzerPeriod) {
	// 			ledcWriteTone(channel, 1000);
	// 			buzzerMillis = currentMillis;

	// 			while (buttonHeld == true){

	// 				//If button is held then it will pause the buzzer
	// 				int pausePeriod = 10000;
	// 				if (currentMillis - buzzerMillis >= pausePeriod) {
	// 					ledcWriteTone(channel, 0);
	// 					buzzerMillis = currentMillis;
	// 				} 	
	// 			} 
	// 		} else {
	// 			ledcWriteTone(channel, 0);
	// 		}
	// 		break;
	// 		}
	// 	}
	// } 
}	 

// LEDState alarms[];

// void buzzer() {
// 	if ( LEDState == AMBER) {
// 		alarms.
// 	} 
// }

void cycleIntervals() {
	pos++;
	if(pos>5){
		pos = 0;
	}
	period = timeIncrements[pos];

	  Serial.print("Update interval: ");
	  Serial.print(period);
	  Serial.println("ms");
}


unsigned long snoozeTimeStartMillis;

void buttonHold(){
	 int buttonVal = digitalRead(BUTTON);


	// button for alarm snoozing
	if(buttonVal == HIGH && alarmSnoozed == false){
		alarmSnoozed = true;
		Serial.println("[Alert] Snoozed For 2 minutes.");
		snoozeTimeStartMillis = currentMillis;
	} 
	if (alarmSnoozed == true && currentMillis - snoozeTimeStartMillis >= 120000 ){
		alarmSnoozed = false;
	}
	// button for interval changing 


	  if(buttonVal == HIGH && buttonDownTime == 0 && (millis() - lastButtonDown) > long(debounce) && buttonHeld == false){
	    buttonDownTime = millis();
	    buttonHeld = true;
	  }

	  if (buttonVal == HIGH && (millis() - buttonDownTime) >= long(holdTime) && buttonHeld == true)
	  {
	    cycleIntervals();
	    lastButtonDown = millis();
	    buttonDownTime = 0;
	    buttonHeld = false;
	  }
}


void sendValuesToServer(){
	 HTTPClient http;
	 String url = "http://172.20.10.2:3000/2post2symposium/" + String(temperature) + "/" + String(humidity) + "/" + PIRState + "/" + humState;

	 //Serial.println(url);

	 http.begin(url);
	    int httpResponseCode = http.GET();
	    if(httpResponseCode > 0) {
	      String response = http.getString();
	      //Serial.println(httpResponseCode);
	      //Serial.println(response);
	    } else {
	      Serial.println("Error");
	    }
	  http.end();
}

#ifdef UNIT_TEST

#else 

void setup()
{
	Serial.begin(115200);
	startMillis = millis();
	tempMillis = millis();
	sdSetup();
	sensorSetup();
	OLED_Setup();
	LED_Setup();
	buzzerSetup();
	pinMode(POTENTIOMETER, INPUT);
	pinMode(BUTTON, INPUT);
	wifiEnabled = wifiSetup();
	Serial.print("System Ready");
	delay(200);
	Serial.print(".");
	delay(200);
	Serial.print(".");
	delay(200);
	Serial.println(".");

}

// The loop function is called in an endless loop
void loop()
{
	currentMillis = millis();
	PIR_Read();
	buttonHold();
	buzzerLoop();
	

	if (currentMillis - tempMillis >= 2000){
		getTemperature();
		tempMillis = currentMillis;
	}

	if( currentMillis - amberAlarmMillis >= 30000 && PIRState == OCCUPIED && amberAlarm == true){
		if (alarmSnoozed != true ){
			ledcWriteTone(channel, 3000);
		} else {
			ledcWriteTone(channel, 0);
		}
		if(currentMillis - amberAlarmMillis >= 32000){
			ledcWriteTone(channel, 0);
			amberAlarmMillis = currentMillis;
		}
	}
	if( currentMillis - redAlarmMillis >= 5000 && PIRState == OCCUPIED && redAlarm == true){
		if (alarmSnoozed != true){
			ledcWriteTone(channel, 3000);
		} else {
			ledcWriteTone(channel, 0);
		}
		if(currentMillis - redAlarmMillis >= 7000){
			ledcWriteTone(channel, 0);
			redAlarmMillis = currentMillis;
		}
	}

	if (currentMillis - startMillis >= period){
		Serial.println("-----------------------------------------------------");
		Serial.print("Speeed: ");
		Serial.println(period);
		
		LED_Status(temperature, humidity);
		displayDHTStats();
		PIR_Status();
		sdCard();
		if((redAlarm == true || amberAlarm == true) && alarmSnoozed == false){
			if(redAlarm == true){
				Serial.println("Red Environmental Alert!");
			} else {
				Serial.println("Amber Environmental Alert!");
			}
		}
		if(wifiEnabled) {
			sendValuesToServer();
		}
		startMillis = currentMillis;
	}
	Screen();
}

#endif
