/*
 * PIRState.h
 *
 *  Created on: 12 Feb 2020
 *      Author: cmparoge
 */

#ifndef PIRSTATE_H_
#define PIRSTATE_H_

enum PIRState {
	VACANT,
	OCCUPIED
};



#endif /* PIRSTATE_H_ */
