/*
 * LEDState.h
 *
 *  Created on: 6 Feb 2020
 *      Author: cmparoge
 */

#ifndef LEDSTATE_H_
#define LEDSTATE_H_

enum LEDState {
	GREEN,
	AMBER,
	RED
};




#endif /* LEDSTATE_H_ */
