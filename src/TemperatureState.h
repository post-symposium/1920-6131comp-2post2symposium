/*
 * temperatureStatus.h
 *
 *  Created on: 6 Feb 2020
 *      Author: cmparoge
 */

#ifndef TEMPERATURESTATUS_H_
#define TEMPERATURESTATUS_H_


enum TemperatureState {
	TEMP_GREEN,
	TEMP_AMBER
};


#endif /* TEMPERATURESTATUS_H_ */
